#include "board.h"

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
    taken.reset();
    black.reset();
    for (int i = 0; i < 64; i++) {
        if (data[i] == 'b') {
            taken.set(i);
            black.set(i);
        } if (data[i] == 'w') {
            taken.set(i);
        }
    }
}

/*
Chooses a depth, give a position and time left - should be even for the heu.
*/
int Board::depthchoose(int msLeft)
{
    int depth;
    int moves = count(BLACK) + count(WHITE);
    if (moves >= 55)
    {
        depth = 20;
    }
    else if(moves < 6)
    {
        depth = 2;
    }
    else if(moves < 12)
    {
	depth = 4;
    }
    else if(moves < 18)
    {
	depth = 5;
    }
    else if(moves < 24)
    {
        depth = 6;
    }
    else
    {
        //int movesleft = (55 - moves) / 2;
        //int timePerMove = msLeft / movesleft;
        if (msLeft > 120000)
        {
            depth = 7;
        }
        else if (msLeft >  60000)
        {
            depth = 6;
        }
        else if (msLeft >  10000)
        {
	    depth = 4;
        }
        else
        {
            depth = 2;
        }
    }
    fprintf(stderr, "%d \n", depth);
    return depth;
}

//Heuristics Evaluates for black, flips at the end if white
//side is the side on the move
//returns + if they're winning
//- if they're losing
int Board::cornerHeuristic(Side side)
{
    int temp = 0;
    temp += (get(BLACK, 0, 0) ? 3: 0);
    temp += (get(WHITE, 0, 0) ? -3: 0);
    
    temp += (get(BLACK, 0, 7) ? 3: 0);
    temp += (get(WHITE, 0, 7) ? -3: 0);
    
    temp += (get(BLACK, 7, 0) ? 3: 0);
    temp += (get(WHITE, 7, 0) ? -3: 0);
    
    temp += (get(BLACK, 0, 0) ? 3: 0);
    temp += (get(WHITE, 7, 7) ? -3: 0);
    
    return (side == BLACK ? temp: -temp);
}

int Board::greedyHeuristic(Side side)
{
    int temp = countBlack() - countWhite();
    return (side == BLACK ? temp: -temp);
}

//Returns score if gameover.
int Board::gameoverHeuristic(Side side)
{
    if (greedyHeuristic(side) > 0)
    {
        return 999;
    }
    else if (greedyHeuristic(side) < 0)
    {
        return -999;
    }
    else
    {
        return 0;
    }
}

//Examines edges
int Board::betterHeuristic(Side side)
{
    int temp = 0;
    
    Side side_on_move = side;
    Side side_not_move = (side == BLACK ? WHITE : BLACK);
    
    if (get(side_on_move, 0, 0))
    {
        temp += 29;
    }
    else if (get(side_not_move, 0, 0))
    {
        temp -= 29;
    }
    else
    {
        temp += (get(side_on_move, 1, 1) ? -5: 0);
        temp += (get(side_not_move, 1, 1) ? 5: 0);
    }
    

    if (get(side_on_move, 0, 7))
    {
        temp += 29;
    }
    else if (get(side_not_move, 0, 7))
    {
        temp -= 29;
    }
    else
    {
        temp += (get(side_on_move, 1, 6) ? -5: 0);
        temp += (get(side_not_move, 1, 6) ? 5: 0);
    }
    
    if (get(side_on_move, 7, 0))
    {
        temp += 29;
    }
    else if (get(side_not_move, 7, 0))
    {
        temp -= 29;
    }
    else
    {
        temp += (get(side_on_move, 6, 1) ? -5: 0);
        temp += (get(side_not_move, 6, 1) ? 5: 0);
    }
    
    if (get(side_on_move, 7, 7))
    {
        temp += 29;
    }
    else if (get(side_not_move, 7, 7))
    {
        temp -= 29;
    }
    else
    {
        temp += (get(side_on_move, 6, 6) ? -5: 0);
        temp += (get(side_not_move, 6, 6) ? 5: 0);
    }

    
    for (int i = 1; i < 7; i++)
    {
        temp += (get(side_on_move, i, 0) ? 3: 0);
        temp += (get(side_not_move, i, 0) ? -3: 0);
    
        temp += (get(side_on_move, 0, i) ? 3: 0);
        temp += (get(side_not_move, 0, i) ? -3: 0);
    
        temp += (get(side_on_move, i, 7) ? 3: 0);
        temp += (get(side_not_move, i, 7) ? -3: 0);
    
        temp += (get(side_on_move, 7, i) ? 3: 0);
        temp += (get(side_not_move, 7, i) ? -3: 0);
    }
    
    return temp;
}

// Stores a value in valueList[depth] for advantage of person on move.
// Returns + if side (side to move) is winning
void Board::minimaxHeuristic(Side side, int depth, Board * depthlist[120], int valueList[120])
{
    //TODO opening tables
    if(depth == 0)
    {
        valueList[0] = betterHeuristic(side);
    }
    else
    {
        Move *myMove = new Move(-1,-1);
        valueList[depth] = -1000;
        int moveList[120][2];
        int guessList[120];
        bool testedList[120] = {false};
        int numMoves = 0;
        // Loop through all possible squares to move.
        Board * myBoard = copy();
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                myMove->x = i;
                myMove->y = j;
                if (checkMove(myMove, side))
                {
                    moveList[numMoves][0] = i;
                    moveList[numMoves][1] = j;
                    myBoard->black = black;
                    myBoard->taken = taken;
                    myBoard->doMove(myMove, side);
                    guessList[numMoves] = myBoard->betterHeuristic(side);
                    
                    numMoves += 1;
                }
            }
        }
        
        //Sort stuff here?
        
        //Loops through all the found moves, starting with the best heuristic.
        for (int i = 0; i < numMoves; i++)
        {
            
            if (-valueList[depth] < valueList[depth + 1])
            {
                // Breaks loop if ab pruned
                break;
            }
            
            int index = -1;
            for (int j = 0; j < numMoves; j++)
            {
                if (!testedList[j])
                {
                    if (index == -1 || guessList[index] < guessList[j])
                    {
                        index = j;
                    }
                }
            }
            testedList[index] = true;
            
            //Now performs the minimax
            myMove->x = moveList[index][0];
            myMove->y = moveList[index][1];

            depthlist[depth]->black = black;
            depthlist[depth]->taken = taken;
            depthlist[depth]->doMove(myMove, side);
            depthlist[depth]->minimaxHeuristic((side == BLACK ? WHITE: BLACK), depth - 1, depthlist, valueList);
            // val represents how good myMove is for other player
            // -val is how good myMove is for me
            if(-valueList[depth - 1] > valueList[depth])
            {
                valueList[depth] = -valueList[depth - 1];
            }
            
            // Now gets rid of this possible move
            //moveList[index][0] = moveList[numMoves - 1][0];
            //moveList[index][1] = moveList[numMoves - 1][1];
            //guessList[index] = guessList[numMoves - 1];
            //numMoves--;
        }
    
        // If there are no moves, we pass.
        if (numMoves == 0)
        {
            depthlist[depth]->black = black;
            depthlist[depth]->taken = taken;
            depthlist[depth]->doMove(NULL, side);
            // Check to see if game is over
            if (!depthlist[depth]->hasMoves(side == BLACK ? WHITE: BLACK))
            {
                valueList[depth] = gameoverHeuristic(side == BLACK ? WHITE: BLACK);
            }
            else
            {
                depthlist[depth]->minimaxHeuristic((side == BLACK ? WHITE: BLACK), depth - 1, depthlist, valueList);
                valueList[depth] = - valueList[depth - 1];
            }
        }
    }
}

/*
Move *Board::openingBook(Side side, Move * last)
{
    if(side == BLACK)
    {
        if(count(BLACK) + count(WHITE) == 4)
        {
            Move * myMove = new Move(2,3);
            return myMove;
        }
    }
    else 
    {
        
        if(last->x == 2 && last->y == 3)
        {
        
        return NULL;
    }  
}
*/
