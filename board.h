#ifndef __BOARD_H__
#define __BOARD_H__

#include <bitset>
#include "common.h"
#include <cstdio>
#include <iostream>
using namespace std;

class Board {
   
private:
    bitset<64> black;
    bitset<64> taken;    
       
    bool occupied(int x, int y);
    bool get(Side side, int x, int y);
    void set(Side side, int x, int y);
    bool onBoard(int x, int y);
      
public:
    Board();
    ~Board();
    Board *copy();
        
    bool isDone();
    bool hasMoves(Side side);
    bool checkMove(Move *m, Side side);
    void doMove(Move *m, Side side);
    int count(Side side);
    int countBlack();
    int countWhite();

    void setBoard(char data[]);
    
    int depthchoose(int msLeft);
    
    int cornerHeuristic(Side side);
    int greedyHeuristic(Side side);
    int betterHeuristic(Side side);
    int gameoverHeuristic(Side side);
    void minimaxHeuristic(Side side, int depth, Board * depthlist[120], int valuelist[120]);
    
    //Move *openingBook(Side side);
};

#endif
