#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include <cstdio>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    Board *myBoard;
    Move *doMove(Move *opponentsMove, int msLeft);
    Side mySide;
    Side theirSide;

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
