#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    mySide = side;
    if (mySide == BLACK)
    {
        theirSide = WHITE;
    }
    else
    {
        theirSide = BLACK;
    }
    
    myBoard = new Board();
}

/*
 * Destructor for the player.
 */
Player::~Player()
{
    delete myBoard;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft)
{
    //TODO depth choose
    bool found_a_move = false;
    if(opponentsMove != NULL)
    {
        //std::cerr << "their move wasn't null" << std::endl;
        myBoard->doMove(opponentsMove, theirSide);
    }
    //std::cerr << "called domove" << std::endl;
    
    /*
    // Look for openingtable
    if (myBoard->openingBook(mySide) != NULL)
    {
        return myBoard->openingBook(mySide);
    }
    */
    
    Board *boardList[120];
    for(int ii = 0; ii < 120; ii ++)
    {
        boardList[ii] = new Board();
    }
    int valueList[120];
    
    Move *myMove = new Move(0, 0);
    Move *bestMove = new Move(0, 0);
    
    int best_value = -1000;
    //Loop through possible moves
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            myMove->x = i;
            myMove->y = j;
            if (myBoard->checkMove(myMove, mySide))
            {
                found_a_move = true;
                //fprintf(stderr, "coordinates: %d %d \n", i, j);
                Board * tempBoard = myBoard->copy();
                tempBoard->doMove(myMove, mySide);
                
                int depth = myBoard->depthchoose(msLeft); // depth choose
                
                for (int k = 0; k < 120; k++)
                {
                    valueList[k] = -1000;
                }
                
                tempBoard->minimaxHeuristic(theirSide, depth, boardList, valueList);
                
                int myMove_value = -valueList[depth];
                
                
                if (myMove_value > best_value)
                {
                    best_value = myMove_value;
                    bestMove->x = i;
                    bestMove->y = j;
                }
                delete tempBoard;
            }
        }
    }
    if (found_a_move)
    {
        myBoard->doMove(bestMove, mySide);
        return bestMove;
    }
    else
    {
        return NULL;
    }
}


/*
{
    bool changed = false;
    if(opponentsMove != NULL)
    {
        //std::cerr << "their move wasn't null" << std::endl;
        myBoard->doMove(opponentsMove, theirSide);
    }
    //std::cerr << "called domove" << std::endl;
    Move *myMove = new Move(0,0);
    Move *bestMove = new Move(0,0);
    int best_value = -1000;
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            myMove->x = i;
            myMove->y = j;
            if (myBoard->checkMove(myMove, mySide))
            {
                //fprintf(stderr, "coordinates: %d %d \n", i, j);
                Board * temp = myBoard->copy();
                temp->doMove(myMove, mySide);
                Move *theirMove = new Move(0,0);
                Move *theirBestMove = new Move(0,0);
                int their_best = -1000;
                int t;
                for(int kk = 0; kk < 8; kk++)
                {
                    for(int mm = 0; mm < 8; mm++)
                    {
                        theirMove->x = kk;
                        theirMove->y = mm;
                        if(temp->checkMove(theirMove, theirSide))
                        {
                            Board * temp2 = temp->copy();
                            temp2->doMove(theirMove, theirSide);
                            if(testingMinimax)
                            {
                                t = temp2->greedyHeuristic(theirSide);
                            }
                            else
                            {
                                t = temp2->cornerHeuristic(theirSide);
                            }
                            if(t > their_best)
                            {
                                theirBestMove->x = kk;
                                theirBestMove->y = mm;
                                their_best = t;
                            }
                        }
                    }
                }
                if(-their_best > best_value)
                {
                    bestMove->x = i;
                    bestMove->y = j;
                    best_value = -their_best;
                    changed = true;
                }
                //
                int h;
                if(testingMinimax)
                {
                    h = temp->greedyHeuristic(mySide);
                }
                else
                {
                    h = temp->cornerHeuristic(mySide);
                }
                
                //fprintf(stderr, "Heuristic: %d \n", h);
                if(h > best_value)
                {
                    //fprintf(stderr, "coordinates: %d %d \n", i, j);
                    bestMove->x = i;
                    bestMove->y = j;
                    best_value = h;
                    //fprintf(stderr, "coordinates: %d %d \n", i, j);
                    changed = true;
                }
                //
            }
        }
    }
    if(changed)
    {
        myBoard->doMove(bestMove, mySide);
        return bestMove;
    }
    else
    {
        return NULL;
    }
}
*/
